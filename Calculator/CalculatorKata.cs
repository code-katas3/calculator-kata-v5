﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class CalculatorKata
    {
        string newLineDelimiter = "\n";
        string forwardSlashDelimiter = "//";
        public int Add(string numbers)
        {
            string[] delimiters = GetDelimiters(numbers);
            numbers = removeDelimiterData(numbers);
            string[] strings = numbers.Split(delimiters, StringSplitOptions.None);
            List<int> numberList = new List<int>();
            foreach (var s in strings)
            {
                if (int.TryParse(s, out int number))
                {
                    numberList.Add(number);
                }
            }
            negativeNumberCheck(numberList);
            IgnoreNumbersGreaterThan1000(numberList);
            return numberList.Sum();
        }

        public string[] GetDelimiters(string numbers)
        {
            if (noDelimiterDataExistsIn(numbers))
            {
                return new string[] { ",", "\n" };
            }

            int delimiterLength = numbers.IndexOf(newLineDelimiter) - 2;
            var delimiterData = numbers.Substring(2, delimiterLength);
            string openingBracket = "[";

            if (delimiterData.Contains(openingBracket))
            {
                string squareBracketDelimiter = "][";
                delimiterData = delimiterData.Substring(1, delimiterData.Length - 2);
                return delimiterData.Split(new string[] { squareBracketDelimiter }, StringSplitOptions.RemoveEmptyEntries);
            }

            var delimiterString = numbers.Substring(2, 1);
            return new string[] { delimiterString };
        }
        private bool noDelimiterDataExistsIn(string numbers)
        {
            return !numbers.StartsWith(forwardSlashDelimiter);
        }

        public void negativeNumberCheck(List<int> numberList)
        {
            var negativeNumbers = numberList.Where(x => x < 0).ToList();
            if (negativeNumbers.Any())
            {
                throw new Exception("Negatives not allowed : " + string.Join(" , ", negativeNumbers));
            }
        }
        public void IgnoreNumbersGreaterThan1000(List<int> numberList)
        {
            var maxNumber = 1000;
            numberList.RemoveAll(x => x > maxNumber);
        }

        private string removeDelimiterData(string numbers)
        {
            if (numbers.StartsWith(forwardSlashDelimiter))
            {
                int startDelimiter = numbers.IndexOf(newLineDelimiter) + 1;
                return numbers.Substring(startDelimiter);
            }

            return numbers;
        }
    }
}
