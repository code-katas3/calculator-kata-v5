using NUnit.Framework;
using Calculator;
using System;

namespace CalculatorTest.tests
{
    public class CalculatorTest
    {
        [Test]
        [TestCase(0, "")]
        public void GIVEN_EmptyOrNullString_WHEN_RETURN0(int expected, string numbers)
        {
            var calc = new CalculatorKata();

            var result = calc.Add(numbers);

            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase(1, "1")]
        [TestCase(2, "2")]
        public void GIVEN_String1And2_WHEN_AddingNumbers_RETURN1And2(int expected, string numbers)
        {

            var calc = new CalculatorKata();

            var result = calc.Add(numbers);

            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase(3, "1,2")]
        public void GIVEN_TwoStrings_WHEN_Adding2Numbers_RETURN_Sum(int expected, string numbers)
        {

            var calc = new CalculatorKata();

            var result = calc.Add(numbers);

            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase(29, "1,2,7,3,6,9,1")]
        [TestCase(11, "1,1,2,3,4")]
        public void GIVEN_MoreNumbers_WHEN_AddingNumbers_RETURN_Sum(int expected, string numbers)
        {

            var calc = new CalculatorKata();

            var result = calc.Add(numbers);

            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase(6, "1\n2,3")]
        public void GIVEN_NewLinesSeparators_WHEN_AddingNumbers_RETURN_Sum(int expected, string numbers)
        {

            var calc = new CalculatorKata();

            var result = calc.Add(numbers);

            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase(3, "//;\n1;2")]
        public void GIVEN_CustomDelimiters_WHEN_AddingNumbers_RETURN_SumOfNumbers(int expected, string numbers)
        {

            var calc = new CalculatorKata();

            var result = calc.Add(numbers);

            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase("-2", "1,-2")]
        public static void GIVEN_NegativeNumbers_WHEN_AddingNumbers_RETURN_ThrowException(string expectedNegatives, string input)
        {

            var calc = new CalculatorKata();

            var exception = Assert.Throws<Exception>(() => calc.Add(input));

            Assert.AreEqual("Negatives not allowed : " + expectedNegatives, exception.Message);
        }

        [Test]
        [TestCase(2, "2,1001")]
        public void GIVEN_NumbersAbove1000_WHEN_AddingTwoPlus1001_RETURN_2(int expected, string input)
        {

            var calc = new CalculatorKata();

            var result = calc.Add(input);

            Assert.AreEqual(expected, result);

        }

        [Test]
        [TestCase(6, "//***\n1***2***3")]
        public void GIVEN_CustomDelimiter_WHEN_AddingNumbers_RETURN_Sum(int expected, string numbers)
        {

            var calc = new CalculatorKata();

            var result = calc.Add(numbers);

            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase(6, "//[*][%]\n1*2%3")]
        public void GIVEN_MultipleCustomDelimiter_WHEN_AddingNumbers_RETURN_Sum(int expected, string numbers)
        {

            var calc = new CalculatorKata();

            var result = calc.Add(numbers);

            Assert.AreEqual(expected, result);
        }
    }
}